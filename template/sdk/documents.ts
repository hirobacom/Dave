/*
 * @Author: your name
 * @Date: 2020-06-19 20:55:30
 * @LastEditTime: 2021-02-12 01:13:54
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \nodec:\Users\zhamgzifang\Desktop\Dave\template\sdk\documents.ts
 */
var relevance = require("../../DaveFile/database/sdkdocuments.json")
function isName(item): boolean {
    return item !== 'query' && item !== 'delete' && item !== 'update' && item !== 'add' && item !== 'queryList'
}
var documentsrender = function (msg: any, name: any, comment: string) {
    var apiParam = ''
    relevance[name] = relevance[name] || {}
    Object.keys(relevance[name]).map(s => {
        if (typeof relevance[name][s] === 'object' && isName(s)) {
            apiParam += val(name, relevance[name], s)
        }
    })
    if (relevance[name].add) {
        var value = relevance[name]
        if (relevance.insertion[name] && relevance.insertion[name].add) {
            value = [...value, ...relevance.insertion[name].add]
        }
        apiParam += val(name, value, 'add')
    } else {
        var value = msg
        if (relevance.insertion[name] && relevance.insertion[name].add) {
            relevance.insertion[name].add.map(s =>{
                s.Field = s.title
                s.Comment = s.msg,
                s.add = true
            })
            value = [...value, ...relevance.insertion[name].add]
        }
        apiParam += yValue(name, comment, value, 'add', '新增')
    }
    if (relevance[name].query) {
        var value = relevance[name]
        if (relevance.insertion[name] && relevance.insertion[name].query) {
            value = [...value, ...relevance.insertion[name].query]
        }
        apiParam += val(name, value, 'query')
    } else {
        var value = msg
        if (relevance.insertion[name] && relevance.insertion[name].query) {
            relevance.insertion[name].query.map(s =>{
                s.Field = s.title
                s.Comment = s.msg,
                s.query = true
            })
            value = [...value, ...relevance.insertion[name].query]
        }
        apiParam += yValue(name, comment, value, 'query', '查询详情')
    }

    if (relevance[name].queryList) {
        var value = relevance[name]
        if (relevance.insertion[name] && relevance.insertion[name].queryList) {
            value = [...value, ...relevance.insertion[name].queryList]
        }
        apiParam += val(name, value, 'queryList')
    } else {
        var value = msg
        if (relevance.insertion[name] && relevance.insertion[name].queryList) {
            relevance.insertion[name].queryList.map(s =>{
                s.Field = s.title
                s.Comment = s.msg,
                s.queryList = true
            })
            value = [...value, ...relevance.insertion[name].queryList]
        }
        apiParam += yValue(name, comment, value, 'queryList', '查询列表')
    }

    if (relevance[name].update) {
        var value = relevance[name]
        if (relevance.insertion[name] && relevance.insertion[name].update) {
            value = [...value, ...relevance.insertion[name].update]
        }
        apiParam += val(name, relevance[name], 'update')
    } else {
        var value = msg
        if (relevance.insertion[name] && relevance.insertion[name].update) {
            relevance.insertion[name].update.map(s =>{
                s.Field = s.title
                s.Comment = s.msg,
                s.update = true
            })
            value = [...value, ...relevance.insertion[name].update]
        }
        apiParam += yValue(name, comment, value, 'update', '更新')
    }

    if (relevance[name].delete) {
        var value = relevance[name]
        if (relevance.insertion[name] && relevance.insertion[name].delete) {
            value = [...value, ...relevance.insertion[name].delete]
        }
        apiParam += val(name, value, 'delete')
    } else {
        var value = msg
        if (relevance.insertion[name] && relevance.insertion[name].delete) {
            relevance.insertion[name].delete.map(s =>{
                s.Field = s.title
                s.Comment = s.msg,
                s.delete = true
            })
            value = [...value, ...relevance.insertion[name].delete]
        }
        apiParam += yValue(name, comment, value, 'delete', '删除')
    }
    return apiParam
}
function successJson(querys, msg) {
    var key = {
        "delete": `
        * @apiSuccessExample Success-Response:
        *      {
            *        status:200,
            *        data:"删除成功"
            *    }
        `,
        update: `
        * @apiSuccessExample Success-Response:
        *{
         *    status:200,
         *    data:"修改成功"
        *}
        `,
        query: msg ? `
        * @apiSuccessExample Success-Response:
         *    对应的字段
         *      {
         *        ${msg.map((s: any) => {
            if (s[querys]) {
                return (`
                                    *            "${s.Field}":"${s.Comment}"
                                `)
            } else {
                return ''
            }
        }).join('')}
         *       }
        
        ` : ''

    }
    return key[querys]
}
function valSucess(msg, state, name) {
    if (!msg[state].success) {
        return ''
    }
    if (msg[state].success === 'all') {
        var documents = require("../../DaveFile/database/documents.json")
        return `
        * @apiSuccessExample Success-Response:
        *    {
        *    ${Object.keys(documents[name]).map(s => {
            return (`
                    *       "${documents[name][s].Field}":"${documents[name][s].Comment}"
                `)
        }).join('')}
    `
        return
    }
    return `
        * @apiSuccessExample Success-Response:
        *    {
        *    ${Object.keys(msg[state].success).map(s => {
        return (`
                    *       "${s}":"${msg[state].success[s]}"
                `)
    }).join('')}
    `
}
// name 对象 状态接口
function val(name, msg, state) {
    var s = `/**
    * @api {post} /${name}/${state} ${msg[state].title}
    * @apiDescription ""
    * @apiName ${state}${name}
    * @apiGroup ${msg.title}
    ${msg[state].props.map((s: any) => {
        return (`
       * @apiParam {string} ${s.title} ${s.msg}
   `)
    }).join('')}
    ${(state === 'query' || state === "queryList") ? `
    * @apiParam {string} page 分页，如果不需要可不发
    * @apiParam {string} pageSize 分页数量` : ''}
    * @apiSuccess {json} code *
     ${valSucess(msg, state, name)}}
    * @apiSampleRequest /${name}/${state}
    * @apiVersion 0.0.0
   */
   `
    return s
}
function yValue(name, comment, msg, state, value) {
    var theName = relevance.TheInterfaceName[name]
    var sv = `
    /**
         * @api {post} /${name}/${state} ${theName && theName[state] ? theName[state] : (`${comment ? comment : name}${value}`)}
         * @apiDescription ""
         * @apiName ${state}${name}
         * @apiGroup ${name}
         ${msg.map((s: any) => {
        if (s[state]) {
            return (`
                     * @apiParam {string} ${s.Field} ${s.required ? '必填-' : ''}${s.Comment}
                 `)
        } else {
            return ''
        }
    }).join('')}
    
    ${(state === 'query' || state === "queryList") ? `
    * @apiParam {string} page 分页，如果不需要可不发
    * @apiParam {string} pageSize 分页数量` : ''}
         * @apiSuccess {json} code
          ${successJson(state, msg)}
         * @apiSampleRequest /${name}/${state}
         * @apiVersion 0.0.0
        */`
    return sv
}
module.exports = documentsrender 